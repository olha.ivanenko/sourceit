import java.util.Scanner;

public class Students {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String[] students = new String[]{"Anna", "Mike", "Jack", "Mia", "Nick"};
        int[] marks = new int[5];
        setGrades(students, marks);
        knowGrades(students, marks);
    }

    public static void setGrades(String[] students, int[] marks) {
        for (int i = 0; i < students.length; i++) {
            System.out.println("Set grade for student " + students[i]);
            marks[i] = scanner.nextInt();
        }
    }

    public static void knowGrades(String[] students, int[] marks) {
        System.out.println("Enter a number of student to know his grade: ");
        int studNumber = scanner.nextInt();
        if (studNumber <= students.length) {
            System.out.println(students[studNumber - 1] + " " + marks[studNumber - 1]);
        }else {
            System.out.println("No student with this number");
        }

        }

    }
