import java.util.Scanner;

public class Eating {
    public static Scanner scanner = new Scanner(System.in);
    static final int MAX_DUMPLINGS_COUNT = 21;

    public static void main(String[] args) {
        eatDumplings();
    }

    public static void eatDumplings() {
        int curDumplingsCount = MAX_DUMPLINGS_COUNT;
        int eatenByUser1, eatenByUser2;

        while (curDumplingsCount > 0) {
            System.out.println("User1: ");
            eatenByUser1 = scanner.nextInt();
            //срабатывает gameUser1 чтобы проверить сколько съел игрок, результат записывается в user1
            eatenByUser1 = eatenDumplings(curDumplingsCount, eatenByUser1);
            curDumplingsCount -= eatenByUser1;
            System.out.println("dumplings left: " + curDumplingsCount);
            if (curDumplingsCount == 0){
                System.out.println("User1, you wash dishes, haha!");
                break;
            }

            System.out.println("User2: ");
            eatenByUser2 = scanner.nextInt();
            eatenByUser2 = eatenDumplings(curDumplingsCount, eatenByUser2);
            curDumplingsCount -= eatenByUser2;
            System.out.println("dumplings left: " + curDumplingsCount);
            if (curDumplingsCount == 0){
                System.out.println("User2, you wash dishes, haha!");
                break;
            }
        }
    }

    //функция для проверки, чтобы пользователь1 съел от 1 до 5 штуки и не больше
    public static int eatenDumplings(int dumplingsCountNow, int eatenDumplings) {
        while (true){
            if(eatenDumplings > 5){
                System.out.println("You can't eat more than 5");
            } else if(eatenDumplings > dumplingsCountNow){
                System.out.println("You've entered more than left");
            } else if(eatenDumplings <= 0){
                System.out.println("You need to eat at least 1");
            } else {
                return eatenDumplings;
            }
            eatenDumplings  = scanner.nextInt();
        }
    }
}
