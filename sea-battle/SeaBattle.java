import java.util.Scanner;

public class SeaBattle {
    public static Scanner scanner = new Scanner(System.in);
    public static final int FIELD_SIZE = 5;

    public static void main(String[] args) {
        Game();
    }

    public static void Game() {
        String[][] fieldTrue = new String[FIELD_SIZE][FIELD_SIZE];
        String[][] fieldFake = new String[FIELD_SIZE][FIELD_SIZE];

        fillfield(fieldTrue, fieldFake);

        int spawedShips = 4;
        int destroyedShips = 4;
        int attempts = 10;
        int x, y;

        System.out.println("Поле:");
        printfield(fieldFake);

        while (spawedShips != 0 && attempts > 0) {
            System.out.println("Введите координаты Х и У (отсчет начинается с (0,0))");
            x = scanner.nextInt();
            y = scanner.nextInt();

            //проверка на выход за границы
            //outOfFields возвращает массив, поэтому я записываю значения в новый массив coordinates[]
            int[] coordinates = outOfField(fieldTrue, x, y);
            x = coordinates[0];
            y = coordinates[1];

            if (fieldTrue[x][y].equals("*")) {
                fieldFake[x][y] = "x";
                System.out.println("Вы убили один корабль.");
                printfield(fieldFake);
                destroyedShips -= 1;
            } else{
                fieldFake[x][y] = "o";
                printfield(fieldFake);
                System.out.println("Мимо");
            }

            attempts -= 1;
            System.out.println("Попыток: " + attempts);
        }
        if (attempts == 0 && spawedShips == 0) {
            System.out.println("Вы победили! Количество убитых корабликов: " + (spawedShips - destroyedShips));
        } else if (attempts == 0) {
            System.out.println("Вы проиграли. Количество убитых корабликов: " + (spawedShips - destroyedShips));
        } else System.out.println("Вы победили! Количество убитых корабликов: " + (spawedShips - destroyedShips));
    }

    public static void fillfield(String[][] fieldTrue, String[][] fieldFake) {
        for (int i = 0; i < fieldTrue.length; i++) {
            for (int j = 0; j < fieldTrue.length; j++) {
                fieldTrue[i][j] = "-";
                fieldFake[i][j] = "-";
            }
        }


        fieldTrue[1][4] = "*";
        fieldTrue[1][1] = "*";
        fieldTrue[3][0] = "*";
        fieldTrue[3][2] = "*";
    }

    public static void printfield(String[][] fieldFake) {
        for (int i = 0; i < fieldFake.length; i++) {
            for (int j = 0; j < fieldFake.length; j++) {
                System.out.print(fieldFake[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static int[] outOfField(String[][] fieldTrue, int x, int y) {
        if (x >= fieldTrue.length || y >= fieldTrue.length) {
            while (x >= fieldTrue.length || y >= fieldTrue.length) {
                System.out.println("Вне границ");
                x = scanner.nextInt();
                y = scanner.nextInt();
            }
        }
        int[] coordinates = new int[]{x, y};
        return coordinates;
    }


}
