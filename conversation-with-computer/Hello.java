import java.util.Random;
import java.util.Scanner;

public class Hello {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Enter your name: ");
        String userName = scanner.nextLine();
        giveGreetings(userName);
    }

    public static void giveGreetings(String userName) {
        String[] greetings = new String[]{"hi!", "hice to see you!", "what's new?",
                "it’s been ages!", "where have you been hiding?"};

        Random r = new Random();
        int numb = r.nextInt(5);

        System.out.println("Please, say 'Hello'!");
        String userSayHello = scanner.nextLine();
        if (userSayHello.equals("Hello")) {
            for (int i = 0; i < greetings.length; i++) {
                if (i == numb) {
                    System.out.println(userName + ", " +greetings[i]);
                }
            }
        } else System.out.println("You have bad manners, mister");

    }


}

